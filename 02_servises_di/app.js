'use strict';

angular.module('diApp', [])

  .controller('CalculatorCtrl', function($scope) {
    $scope.product = {
      pageSize: 'A5',
      numberOfPages: 50
    };

    $scope.getPrice = function() {
      var product = $scope.product;
      var price = 0;

      switch (product.pageSize) {
        case 'A4':
          price += 110;
          break;
        case 'A5':
          price += 90;
          break;
        case 'A6':
          price += 70;
          break;
      }

      var pagesPrice = Math.ceil(product.numberOfPages / 5) * 3;
      price += pagesPrice;

      return price;
    }
  });