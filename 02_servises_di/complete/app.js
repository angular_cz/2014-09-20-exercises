'use strict';

angular.module('diApp', [])
  .constant('CALCULATOR_CONFIG', {
    priceForFivePages: 3,
    priceA4: 110,
    priceA5: 90,
    priceA6: 70
  })

  .value('defaultProduct', {
    pageSize: 'A5',
    numberOfPages: 50
  })

  .service('CalculatorService', function(CALCULATOR_CONFIG) {
    return {
      getPrice: function(product) {
        var price = 0;

        switch (product.pageSize) {
          case 'A4':
            price += CALCULATOR_CONFIG.priceA4;
            break;
          case 'A5':
            price += CALCULATOR_CONFIG.priceA5;
            break;
          case 'A6':
            price += CALCULATOR_CONFIG.priceA6;
            break;
        }

        var pagesPrice = Math.ceil(product.numberOfPages / 5) * CALCULATOR_CONFIG.priceForFivePages;
        price += pagesPrice;

        return price;
      }
    };
  })

  .controller('CalculatorCtrl', function($scope, defaultProduct, CalculatorService) {
    $scope.product = angular.copy(defaultProduct);

    $scope.getPrice = function() {
      return CalculatorService.getPrice($scope.product);
    };
  });