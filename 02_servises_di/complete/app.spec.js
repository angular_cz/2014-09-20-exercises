'use strict';

/* jasmine specs for services go here */

describe('service', function() {

  beforeEach(module('diApp'));

  var productA550 = {
    pageSize: 'A5',
    numberOfPages: 50
  };

  describe('CalculatorService', function() {

    it('should count product A5 50 pages correctly', inject(function(CalculatorService) {

      expect(CalculatorService.getPrice(productA550)).toEqual(120);
    }));

  });


});
