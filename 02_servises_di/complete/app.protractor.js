'use strict';

describe('calculator app', function() {

  browser.get('index.html');

  it('should have initial settings', function() { 
    var numberOfPages = element(by.model('product.numberOfPages'));
    expect(numberOfPages.getAttribute('value')).toMatch(50);
  });
  
  it('default model price is proper', function () {    
    expect(element(by.id('product-price')).getText()).toBe('120 Kč');    
  });
});
