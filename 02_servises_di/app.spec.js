'use strict';

/* jasmine specs for services go here */

describe('service', function() {

  beforeEach(module('diApp'));

  var productA550 = {
    pageSize: 'A5',
    numberOfPages: 50
  };

  describe('Calculator', function() {

    it('should count product A5 50 pages correctly', inject(function(Calculator) {

      expect(Calculator.getPrice(productA550)).toEqual(120);
    }));

  });


});
