angular.module('exercise1', [])
  .controller('userController', function($scope) {
    $scope.user = {
      firstname: 'Milan',
      surname: 'Lempera'
    };

    $scope.getFullName = function() {
      console.log('call getFullName');
      return $scope.user.firstname + ' ' + $scope.user.surname;
    };

  });