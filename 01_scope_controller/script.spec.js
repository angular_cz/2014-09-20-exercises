'use strict';

describe('exercise1 module', function() {
  var $scope,
    userController;

  beforeEach(module('exercise1'));

  beforeEach(inject(function($rootScope, $controller) {
    $scope = $rootScope.$new();
    userController = $controller('userController', {$scope: $scope});
  }));

  describe('user controller', function() {

    it('should be defined', inject(function() {
      expect(userController).toBeDefined();
    }));

    it('should define fullname getter', inject(function() {
      expect($scope.getFullName()).toBe('//TODO doplňte hodnotu');
    }));

  });
});