angular.module('FormApp', ['ngMessages', 'ngStorage']).
  controller('UserCtrl', function($scope, userStorage) {
    $scope.userStorage = userStorage;

    $scope.clear = function() {
      userStorage.clear();
      $scope.reset();
    };

    $scope.save = function() {
      if ($scope.userForm.$invalid) {
        return;
      }

      userStorage.save($scope.user);
    };

    $scope.reset = function() {
      $scope.user = userStorage.get();
    };

    $scope.reset();
  })
  .directive('validateCzechPhoneNumber', function() {
    var pattern = /^(\+420)?( ?\d{3}){3}$/;

    return {
      restrict: 'A',
      require: 'ngModel',
      link: function(scope, element, attrs, ngModel) {
        ngModel.$validators.CZPhoneNumber = function(value) {
          return pattern.test(value);
        };
      }
    };
  })
  .service('userStorage', function($localStorage) {
    this.userStorage = $localStorage.$default({
      user: {
        name: 'unknown name'
      }
    });

    this.get = function() {
      return angular.copy(this.userStorage.user);
    };

    this.save = function(user) {
      this.userStorage.user = angular.copy(user);
    };

    this.clear = function() {
      this.userStorage.$reset({
        user: {}
      });
    };

  });