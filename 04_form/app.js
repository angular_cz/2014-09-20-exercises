angular.module('FormApp', ['ngMessages', 'ngStorage']).
  controller('UserCtrl', function($scope, userStorage) {
  })
  .service('userStorage', function($localStorage) {
    this.userStorage = $localStorage.$default({
      user: {
        name: 'unknown name'
      }
    });

    this.get = function() {
      return this.userStorage.user;
    };

    this.save = function(user) {
      this.userStorage.user = user;
    };

    this.clear = function() {
      this.userStorage.$reset({
        user: {}
      });
    };

  });