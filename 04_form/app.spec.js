describe('directives', function() {
  var $scope, form;

  beforeEach(module('FormApp'));

  beforeEach(inject(function($compile, $rootScope) {
    $scope = $rootScope;
    var element = angular.element(
      '<form name="form">' +
      '<input ng-model="model.phone" name="phone" validate-czech-phone-number />' +
      '</form>'
      );

    $scope.model = {phone: null};

    $compile(element)($scope);
    form = $scope.form;
  }));

  describe('integer', function() {

    it('should not pass with string', function() {
      form.phone.$setViewValue('some string');
      $scope.$digest();
      expect($scope.model.phone).toBeUndefined();
      expect(form.phone.$valid).toBe(false);
    });

    it('should pass with phone number', function() {
      form.phone.$setViewValue('+420 123 456 789');
      $scope.$digest();
      expect($scope.model.phone).toEqual('+420 123 456 789');
      expect(form.phone.$valid).toBe(true);
    });
  });

});