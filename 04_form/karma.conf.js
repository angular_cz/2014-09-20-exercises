module.exports = function(config) {
  config.set({
    basePath: '../',
    files: [
      'bower_components/angular/angular.js',
      'bower_components/angular-mocks/angular-mocks.js',
      'bower_components/angular-messages/angular-messages.js',
      'bower_components/angular-cookies/angular-cookies.js',
      'bower_components/ngstorage/ngStorage.js',
      '04_form/app.js',
      '04_form/*.spec.js'
    ],
    frameworks: ['jasmine'],
    browsers: ['Chrome'],
    plugins: [
      'karma-chrome-launcher',
      'karma-firefox-launcher',
      'karma-jasmine',
      'karma-junit-reporter'
    ],
    autoWatch: false,
    singleRun: true,
    junitReporter: {
      outputFile: 'test_out/unit.xml',
      suite: 'unit'
    }

  });
};