angular.module('directiveApp', [])
        .controller('CabinCtrl', function($scope, $http) {
          $http.get('data.json').success(function(data) {
            $scope.cabins = data;
          });

        })
        .directive("cabin", function() {
          return {
            templateUrl: "partial/cabin.html"
          };
        })
        .directive("ratings", function() {
          return {
            restrict: 'E',
            transclude: true,
            replace: true,
            scope: {},
            templateUrl: "partial/ratings.html",
            controller: function($scope) {
              var count = 0;
              var sum = 0;

              this.addRating = function(rating) {
                count++;
                sum += parseInt(rating);
              };

              $scope.getPercentage = function() {
                return (sum / (count * 5)) * 100;
              };

            }
          };
        })
        .directive("rating", function() {
          return {
            restrict: 'E',
            require: "^?ratings",
            replace: true,
            scope: {
              score: "@"
            },
            transclude: true,
            templateUrl: "partial/rating.html",
            link: function(scope, element, attrs, ratingsCtrl) {
              if (ratingsCtrl) {
                ratingsCtrl.addRating(parseInt(scope.score));
              }
            }
          };

        })
        .filter('roundUp', function() {
          return function(price) {
            return Math.ceil(price / 100) * 100; 
          };
        });


   